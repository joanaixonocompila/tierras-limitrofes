package com.example.aplicacio5;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

public class Preferencies extends Activity {
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenciesFragment()).commit();
        setContentView(R.layout.preferencies);
    }
}

/*

SharedPreferences pref=
      PreferenceManager.getDefaultSharedPreferences(this);
String s="text1: "+ pref.getBoolean("key1", false)+
         "\ntext2: "+ pref.getString("key2", "?");

A l’objecte pref se li assigna les preferències definides per a l’aplicació.
Els mètodes get ens permeten obtenir una preferència concreta en funció del valor de la clau.
El segon argument del mètode get és el valor assignat per defecte en el cas de no trobar la clau.

 */