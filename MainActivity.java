package com.example.aplicacio5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button bAcercaDe;
    private Button bConfig;
    private Button bSalir;
    private Button bJugar;

	// Comentari documentacio MainActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Boto Informacio
        bAcercaDe = findViewById(R.id.button3);
        bAcercaDe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                llancarAcercaDe(null);
            }
        });

        // Boto preferencies
        bConfig = findViewById(R.id.button2);
        bConfig.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                llancarPreferencies(null);
            }
        });



        // Boto sortir
        bSalir = findViewById(R.id.button4);
        bSalir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void llancarAcercaDe(View view) {
        Intent i = new Intent(this, AcercaDe.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.config) {
            llancarPreferencies(null);
            return true;
        }

        if(id == R.id.acercaDe) {
            llancarAcercaDe(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void llancarPreferencies(View view) {
        Intent i = new Intent(this, Preferencies.class);
        startActivity(i);
    }

}
